#include <iostream>

#include <systemd/sd-journal.h>
#include <systemd/sd-id128.h>


#define EXAMPLE_MESSAGE SD_ID128_MAKE(69,fb,66,e0,50,3d,48,df,8b,6a,81,98,1a,30,d3,d7)

using namespace std;

int main(int argc, char *argv[]) {
    char messageId[33];
    sd_id128_to_string(EXAMPLE_MESSAGE, messageId);
    sd_journal_send(
            "MESSAGE_ID=%s", messageId,
            "MESSAGE=Message with the example message id",
            "PRIORITY=%i", LOG_ERR,
            nullptr);
}
