find_package(PkgConfig)

pkg_check_modules(
        Systemd REQUIRED
        IMPORTED_TARGET
        libsystemd)

add_executable(logging-in-systemd-1
        logging-in-systemd-1.cpp)

target_link_libraries(logging-in-systemd-1
        PkgConfig::Systemd)

add_executable(logging-in-systemd-2
        logging-in-systemd-2.cpp)

target_link_libraries(logging-in-systemd-2
        PkgConfig::Systemd)

add_executable(logging-in-systemd-3
        logging-in-systemd-3.cpp)

target_link_libraries(logging-in-systemd-3
        PkgConfig::Systemd)
