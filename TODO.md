# TODO List
 - sd-journal library
 - systemd machined, systemd-nspawn:
    - https://blog.selectel.com/systemd-containers-introduction-systemd-nspawn/
    - https://www.freedesktop.org/wiki/Software/systemd/machined/
 - hostnamed
 - localed
 - timedated
 - Systemd/Linux :) (?)
 - Remote logging
 - Communicate status to systemd
 - Watchdog 
 - Cloud init file to create lxc container to run examples
 - How to use the examples
 - SysV daemons vs. new-Style daemons as described in https://www.freedesktop.org/software/systemd/man/daemon.html
 - Modularize the examples for easier inclusion in text
 - File hierarchy overview: https://www.freedesktop.org/software/systemd/man/file-hierarchy.html  
 